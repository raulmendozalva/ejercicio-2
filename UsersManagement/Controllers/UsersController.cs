﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UsersManagement.Models;

namespace UsersManagement.Controllers
{
    public class UsersController : ApiController
    {
        // GET: api/Users
        public IEnumerable<Models.Users> Get()
        {
            using (USERSDBConn DBCOnn = new USERSDBConn())
            {
                return DBCOnn.Users.ToList();
            }
        }
       
        // POST: api/Users
        public HttpResponseMessage Post([FromBody]Users user)
        {
            int response = 0;
            HttpResponseMessage msg = null;

            try
            {
                using (USERSDBConn DBCOnn = new USERSDBConn())
                {
                    DBCOnn.Entry(user).State = System.Data.Entity.EntityState.Added;
                    response = DBCOnn.SaveChanges();
                    msg = Request.CreateResponse(HttpStatusCode.OK, response);
                }
            }
            catch (Exception ex)
            {
                msg.RequestMessage.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return msg;
        }
        
    }
}
